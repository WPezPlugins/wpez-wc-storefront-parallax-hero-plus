<?php
/*
Plugin Name: WPezPlugins: Theme Customize: TODO
Plugin URI: https://gitlab.com/WPezPlugins/wpez-theme-customize
Description: Boilerplate for customizations of your theme / parent theme.
Version: 0.0.2
Author: Mark "Chief Alchemist" Simchock for Alchemy United
Author URI: https://AlchemyUnited.com
License: GPLv2 or later
Text Domain: TODO
*/

namespace WPezWCStorefrontParallaxHeroPlus\App;

use WPezWCStorefrontParallaxHeroPlus\App\Core\HooksRegister\ClassHooksRegister;
use  WPezWCStorefrontParallaxHeroPlus\App\Plugin\Container\ClassContainer;
use WPezWCStorefrontParallaxHeroPlus\App\Core\BackgroundImageStyle\ClassBackgroundImageStyle as BIS;
use WPezWCStorefrontParallaxHeroPlus\App\Plugin\Plus\ClassPlus;

class ClassPlugin {

    protected $_new_plus;

    protected $_arr_actions;

    protected $_arr_filters;

    protected $_new_wpcore;


    public function __construct() {

        $this->setPropertyDefaults();

	    $this->filters( true     );

        $this->actions( true );

        // this should be last
        $this->hooksRegister();

    }

    protected function setPropertyDefaults() {

        $this->_new_hooks_reg = new ClassHooksRegister();
        $this->_arr_actions   = [];
        $this->_arr_filters   = [];


        $new_bis = new BIS();

	    $this->_new_plus = new ClassPlus( $new_bis );
    }

    public function setSetters(){

	    $new_container = new ClassContainer();

	    $this->_new_plus->setChoices($new_container->newSettings->get('_arr_padding_choices'));
	    $this->_new_plus->setChoicesDefault($new_container->newSettings->get('_str_padding_default'));
	    $this->_new_plus->setPaddingUnit($new_container->newSettings->get('_str_padding_unit'));
	    $this->_new_plus->setXS($new_container->newSettings->get('_str_parallax_img_xs'));
	    $this->_new_plus->setMD($new_container->newSettings->get('_str_parallax_img_md'));
	    $this->_new_plus->setLG($new_container->newSettings->get('_str_parallax_img_lg'));

    }

    /**
     * After gathering (below) the arr_actions and arr_filter, it's time to
     * make some RegisterHook magic
     */
    protected function hooksRegister() {

        $this->_new_hooks_reg->loadActions( $this->_arr_actions );

        $this->_new_hooks_reg->loadFilters( $this->_arr_filters );

        $this->_new_hooks_reg->actionRegister();

    }


    public function actions( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }

        $this->_arr_actions[] = [
            'active'    => true,
            'hook'      => 'customize_register',
            'component' =>  $this->_new_plus,
            'callback'  => 'customizeRegisterParallax',
            'priority' => 50
        ];

        // Right before we render let's set our setters
	    $this->_arr_actions[] = [
		    'active'    => true,
		    'hook'      => 'sph_content_after',
		    'component' =>  $this,
		    'callback'  => 'setSetters',
		    'priority' => 20
	    ];

	    $this->_arr_actions[] = [
		    'active'    => true,
		    'hook'      => 'sph_content_after',
		    'component' =>  $this->_new_plus,
		    'callback'  => 'sphContentAfter',
		    'priority' => 30
	    ];

    }

    public function filters( $bool = true ) {

        if ( $bool !== true ) {
            return;
        }


        $this->_arr_filters[] = [
            'hook'      => 'shortcode_atts_parallax_hero',
            'component' =>  $this->_new_plus,
            'callback'  => 'shortcodeAttsParallaxHero',
            'priority' => 20,
            'accepted_args' => 4
        ];
    }

}