<?php

namespace WPezWCStorefrontParallaxHeroPlus\App\Plugin\Plus;

class ClassPlus {

	use \WPezWCStorefrontParallaxHeroPlus\App\Core\Traits\Setters\TraitSetArray;
	use \WPezWCStorefrontParallaxHeroPlus\App\Core\Traits\Setters\TraitSetString;

	protected $_new_bis;
	protected $_arr_choices;
	protected $_str_choices_default;
	protected $_str_image_id;
	protected $_str_padding;
	protected $_str_padding_unit;
	protected $_str_xs;
	protected $_str_md;
	protected $_str_lg;


	public function __construct( $new_bis ) {

		$this->_new_bis = $new_bis;

		$this->setPropertyDefaults( $new_bis );

	}

	protected function setPropertyDefaults( $new_bis ) {

		$this->_arr_choices = [
			'1'  => '1',
			'2'  => '2',
			'3'  => '3',
			'4'  => '4',
			'5'  => '5',
			'6'  => '6',
			'7'  => '7',
			'8'  => '8',
			'9'  => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		];

		$this->_str_choices_default = '6';
		$this->_str_image_id        = false;

		$this->_str_padding_unit = 'em';

		$this->_str_xs = 'full';
		$this->_str_md = 'full';
		$this->_str_lg = 'full';

	}

	public function setChoices( $arr = false ) {

		return $this->setArray( '_arr_choices', $arr );

	}

	public function setChoicesDefault( $str = false ) {

		return $this->setString( '_str_choices_default', $str );
	}

	public function setPaddingUnit( $str = false ) {

		return $this->setString( '_str_padding_unit', $str );
	}

	public function setXS( $str = false ) {

		return $this->setString( '_str_xs', $str );
	}

	public function setMD( $str = false ) {

		return $this->setString( '_str_md', $str );
	}

	public function setLG( $str = false ) {

		return $this->setString( '_str_lg', $str );
	}

	/**
	 * Use the shortcode_atts_* filter to grab some values to be passed to
	 * sphContentAfter()
	 *
	 * @param $out
	 * @param $pairs
	 * @param $atts
	 * @param $shortcode
	 *
	 * @return mixed
	 */
	public function shortcodeAttsParallaxHero( $out, $pairs, $atts, $shortcode ) {

		if ( isset ( $out['background_image'] ) && ! empty( $out['background_image'] ) ) {
			$this->_str_image_id = $out['background_image'];
			//	$out['background_image'] = '';
		}

		if ( isset ( $atts['padding'] ) ) {

			$this->_str_padding = $atts['padding'];

		} else {

			$this->_str_padding = get_theme_mod( 'sph_hero_padding', $this->_str_choices_default );
		}

		return $out;
	}

	public function customizeRegisterParallax( $wp_customize ) {

		$wp_customize->add_setting( 'sph_hero_padding', [
			'default' => $this->_str_choices_default,
		] );


		$wp_customize->add_control( 'sph_hero_padding', [
				'label'    => __( 'Padding', 'wpez-wc-sph-plus' ),
				'description' => __( 'Padding effects the overall height of the parallax window.', 'wpez-wc-sph-plus' ),
				'section'  => 'sph_section_layout',
				'settings' => 'sph_hero_padding',
				'type'     => 'select',
				'priority' => 85,
				'choices'  => $this->_arr_choices
			]
		);

	}

	public function sphContentAfter() {

		if ( is_string( $this->_str_image_id ) ) {

			$this->_new_bis->setAttachmentID( $this->_str_image_id );
			$this->_new_bis->setSelector( 'sph-hero' );
			$this->_new_bis->setSelectorPrefix( '.' );
			$this->_new_bis->pushXS( $this->_str_xs, true );
			$this->_new_bis->pushMD( $this->_str_md, true );
			$this->_new_bis->pushLG( $this->_str_lg, true );
			$this->_new_bis->getStyle( true );

		}

		$str_int = (integer) $this->_str_padding;
		echo '<style>';
		echo '@media screen and (min-width: 768px){.sph-hero .overlay {padding: ' . esc_attr( $str_int ) . esc_attr( $this->_str_padding_unit ) . '; } }';
		echo '</style>';
	}
}