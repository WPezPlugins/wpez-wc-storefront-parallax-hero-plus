<?php
/**
 * Created by PhpStorm.
 * User: mark
 * Date: 3/1/2019
 * Time: 10:28 AM
 */

namespace WPezWCStorefrontParallaxHeroPlus\App\Plugin\Container;

use WPezWCStorefrontParallaxHeroPlus\App\Plugin\Container\Contents\ClassSettings as Settings;

class ClassContainer {

    protected $_new_settings;


    public function __construct() {
    }


    public function __get($str_request){

        switch($str_request){

            case 'newSettings':
                return $this->getSettings();

            default:
                // TODO exception
        }
    }

    protected function getSettings(){

        if ( ! is_object($this->_new_settings )){
            $this->_new_settings = new Settings();
        }
        return $this->_new_settings;
    }


}