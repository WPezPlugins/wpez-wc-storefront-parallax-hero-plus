<?php

namespace WPezWCStorefrontParallaxHeroPlus\App\Plugin\Container\Contents;


class ClassSettings {


	protected $_arr_padding_choices;
	protected $_str_padding_default;
	protected $_str_padding_unit;
	protected $_str_parallax_img_xs;
	protected $_str_parallax_img_md;
	protected $_str_parallax_img_lg;


	public function __construct() {

		$this->setProperyDefaults();
	}

	public function get( $str_property = false, $mix_defaults = '' ) {

		$str_property = trim( strtolower( $str_property ) );

		if ( property_exists( $this, $str_property ) ) {

			return $this->$str_property;
		}

	}

	protected function setProperyDefaults() {

		$arr_choices = [
			'1'  => '1',
			'2'  => '2',
			'3'  => '3',
			'4'  => '4',
			'5'  => '5',
			'6'  => '6',
			'7'  => '7',
			'8'  => '8',
			'9'  => '9',
			'10' => '10',
			'11' => '11',
			'12' => '12',
		];

		$this->_arr_padding_choices = apply_filters( 'wc_parallax_hero_plus_padding_choices', $arr_choices );

		$str_padding_default        = '6';
		$this->_str_padding_default = apply_filters( 'wc_parallax_hero_plus_padding_default', $str_padding_default );

		$str_padding_unit        = 'em';
		$this->_str_padding_unit = apply_filters( 'wc_parallax_hero_plus_padding_unit', $str_padding_unit );

		$str_parallax_img_xs        = 'medium_large';
		$this->_str_parallax_img_xs = apply_filters( 'wc_parallax_hero_plus_img_xs', $str_parallax_img_xs );

		$str_parallax_img_md        = 'large';
		$this->_str_parallax_img_md = apply_filters( 'wc_parallax_hero_plus_img_md', $str_parallax_img_md );

		$str_parallax_img_lg        = 'full';
		$this->_str_parallax_img_lg = apply_filters( 'wc_parallax_hero_plus_img_lg', $str_parallax_img_lg );

	}
}