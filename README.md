## WPezPlugins: WooCommerce Storefront Parallax Hero Plus

__An add-on to the WordPress WooCommerce Storefront Parallax Hero plugin. Minor tweaks with major benefits.__

The key plus is that different (WordPress) image sizes are used for different breakpoints. The WC's original Parallax Hero plugin uses the same (full size) image regardless of viewport. 



> --
>
> Special thanks to JetBrains (https://www.jetbrains.com/) and PhpStorm (https://www.jetbrains.com/phpstorm/) for their support of OSS and its devotees. 
>
> --


### Overview

Plus #1 - As mentioned, there is a better connection between breakpoint / viewport and image size. 

Plus #2 - Under Appearance > Customize > Parallax Hero > Layout you'll not find a select for Padding. By adjusting the padding you can effectively change the height of the parallex window. This setting is applied to (min-width: 768px). Below 768px the plugin's original CSS setting remains.



### FAQ

__1 - Why?__

Serving up an excessively large image to a small screen is just not a good UX. Being able to adjust the padding was ez enough to implement and seemed worth the effort.  

__2 - What about filters?__

Look in App / Plugin / Container / Contents / ClassSettings.php. 


 __#3 - I'm not a developer, can we hire you?__
 
 Yes, that's always a possibility. If I'm available, and there's a good mutual fit. 



### HELPFUL LINKS

- https://woocommerce.com/products/storefront-parallax-hero/ 
 
 
### TODO

- TBD


### CHANGE LOG

- v0.0.1 - 22 March 2019

- INIT - hey! ho! let's go!!!



